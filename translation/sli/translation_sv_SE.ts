<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>QObject</name>
    <message>
        <source>lcd 1.0.1 Copyright (C) 2018 Ingemar Ceicer.</source>
        <oldsource>Copyright (C) 2018 Ingemar Ceicer.</oldsource>
        <translation type="obsolete">Upphovsrätt (C) 2018 Ingemar Ceicer.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="76"/>
        <source>Copyright (C) 2018 Ingemar Ceicer.</source>
        <translation>Upphovsrätt (C) 2018 Ingemar Ceicer</translation>
    </message>
    <message>
        <location filename="main.cpp" line="77"/>
        <source>This is free software; see the GNU General Public Licence</source>
        <translation>Detta är fri programvara; se GNU General Public License</translation>
    </message>
    <message>
        <location filename="main.cpp" line="78"/>
        <source>for copying conditions. There is NO warranty.</source>
        <translation>för kopieringsvillkor. Det finns ingen garanti.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="79"/>
        <source>Least Common Denominator (LCD)</source>
        <translation>Minsta Gemensamma Nämnare (MGN)</translation>
    </message>
    <message>
        <location filename="main.cpp" line="80"/>
        <source>Enter the number of fractional numbers</source>
        <translation>Ange antalet bråktal</translation>
    </message>
    <message>
        <location filename="main.cpp" line="92"/>
        <location filename="main.cpp" line="95"/>
        <source>Fraction </source>
        <translation>Bråktal </translation>
    </message>
    <message>
        <location filename="main.cpp" line="92"/>
        <source>: numerator :&gt;</source>
        <translation>: täljare :&gt;</translation>
    </message>
    <message>
        <location filename="main.cpp" line="95"/>
        <source>: denominator :&gt;</source>
        <translation>: nämnare :&gt;</translation>
    </message>
    <message>
        <location filename="main.cpp" line="153"/>
        <source>LCD</source>
        <translation>MGN</translation>
    </message>
    <message>
        <location filename="main.cpp" line="172"/>
        <location filename="main.cpp" line="193"/>
        <location filename="main.cpp" line="195"/>
        <location filename="main.cpp" line="201"/>
        <location filename="main.cpp" line="203"/>
        <source>Sum</source>
        <translation>Summa</translation>
    </message>
    <message>
        <location filename="main.cpp" line="184"/>
        <source>Shortens... divided by </source>
        <oldsource>Shortens ... divided by </oldsource>
        <translation>Förkortar... deviderar med </translation>
    </message>
    <message>
        <location filename="main.cpp" line="184"/>
        <source> gets... </source>
        <translation> ger... </translation>
    </message>
</context>
</TS>
