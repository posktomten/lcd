<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>CheckForUpdates</name>
    <message>
        <location filename="checkforupdates.cpp" line="40"/>
        <source>No Internet connection was found.
Please check your Internet settings and firewall.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="checkforupdates.cpp" line="40"/>
        <source>LCD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="checkforupdates.cpp" line="46"/>
        <source>
There is a later version of </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="checkforupdates.cpp" line="46"/>
        <source>Please download from</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="checkforupdates.cpp" line="46"/>
        <source>Latest version: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="checkforupdates.cpp" line="50"/>
        <source>
Your version of </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="checkforupdates.cpp" line="50"/>
        <source> is later than the latest official version </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="checkforupdates.cpp" line="54"/>
        <source>
You have the latest version of </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="checkforupdates.cpp" line="58"/>
        <source>
There was an error when the version was checked.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="75"/>
        <location filename="mainwindow.ui" line="185"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="82"/>
        <location filename="mainwindow.ui" line="190"/>
        <source>LCD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="89"/>
        <location filename="mainwindow.ui" line="200"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="157"/>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="166"/>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="173"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="195"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="209"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="218"/>
        <source>Swedish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="227"/>
        <source>Check for updates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="236"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="86"/>
        <source>Source code
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="86"/>
        <source> Copyright (C) 2018 </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="86"/>
        <source> Ingemar Ceicer. This is free software; see the GNU General Public Licence for copying conditions. There is NO warranty.
Least Common Denominator (LCD)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="156"/>
        <source>LCD = </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="157"/>
        <source>Adds the fractional numbers
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="182"/>
        <location filename="mainwindow.cpp" line="205"/>
        <location filename="mainwindow.cpp" line="207"/>
        <location filename="mainwindow.cpp" line="213"/>
        <location filename="mainwindow.cpp" line="215"/>
        <source>Sum = </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="194"/>
        <source>Shortens... divided by </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="194"/>
        <source> gets... </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="translate.cpp" line="30"/>
        <location filename="translate.cpp" line="55"/>
        <source>The program must be restarted for the new language settings to take effect.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="translate.cpp" line="31"/>
        <location filename="translate.cpp" line="56"/>
        <source>Restart Now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="translate.cpp" line="32"/>
        <location filename="translate.cpp" line="57"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="system.cpp" line="23"/>
        <source>This program uses Qt version </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="system.cpp" line="23"/>
        <source> running on </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="system.cpp" line="25"/>
        <source> was created </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="system.cpp" line="25"/>
        <source>by a computer with</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="system.cpp" line="34"/>
        <location filename="system.cpp" line="36"/>
        <location filename="system.cpp" line="86"/>
        <source>Compiled by</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="system.cpp" line="42"/>
        <source>Full version number </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="system.cpp" line="82"/>
        <source>Unknown version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="system.cpp" line="88"/>
        <source>Unknown compiler.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
