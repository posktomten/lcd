<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>CheckForUpdates</name>
    <message>
        <location filename="checkforupdates.cpp" line="40"/>
        <source>No Internet connection was found.
Please check your Internet settings and firewall.</source>
        <translation>Ingen internetanslutning hittas
Kolla dina nätverksinställningar och brandvägg.</translation>
    </message>
    <message>
        <location filename="checkforupdates.cpp" line="40"/>
        <source>LCD</source>
        <translation>MGN</translation>
    </message>
    <message>
        <location filename="checkforupdates.cpp" line="46"/>
        <source>
There is a later version of </source>
        <translation>
Det finns en senare version av </translation>
    </message>
    <message>
        <location filename="checkforupdates.cpp" line="46"/>
        <source>Please download from</source>
        <translation>Ladda ner från</translation>
    </message>
    <message>
        <location filename="checkforupdates.cpp" line="46"/>
        <source>Latest version: </source>
        <translation>Senaste versionen: </translation>
    </message>
    <message>
        <location filename="checkforupdates.cpp" line="50"/>
        <source>
Your version of </source>
        <translation>
Din version av </translation>
    </message>
    <message>
        <location filename="checkforupdates.cpp" line="50"/>
        <source> is later than the latest official version </source>
        <translation> är nyare än den senaste officiella versionen </translation>
    </message>
    <message>
        <location filename="checkforupdates.cpp" line="54"/>
        <source>
You have the latest version of </source>
        <translation>
Du har den senaste versionen av </translation>
    </message>
    <message>
        <location filename="checkforupdates.cpp" line="58"/>
        <source>
There was an error when the version was checked.</source>
        <translation>
Det uppstod ett fel när versionen kontrollerades.</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="75"/>
        <location filename="mainwindow.ui" line="185"/>
        <source>Add</source>
        <translation>Lägg till</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="82"/>
        <location filename="mainwindow.ui" line="190"/>
        <source>LCD</source>
        <translation>MGN</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="89"/>
        <location filename="mainwindow.ui" line="200"/>
        <source>Clear</source>
        <translation>Rensa</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="157"/>
        <source>File</source>
        <translation>Arkiv</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="166"/>
        <source>Language</source>
        <translation>Språk</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="173"/>
        <source>Help</source>
        <translation>Hjälp</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="195"/>
        <source>Exit</source>
        <translation>Avsluta</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="209"/>
        <source>English</source>
        <translation>Engelska</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="218"/>
        <source>Swedish</source>
        <translation>Svenska</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="227"/>
        <source>Check for updates</source>
        <translation>Kolla efter uppdateringar</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="236"/>
        <source>About</source>
        <translation>Om</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="86"/>
        <source>Source code
</source>
        <translation>Källkod
</translation>
    </message>
    <message>
        <source>LCD </source>
        <oldsource>lcd </oldsource>
        <translation type="vanished">MGN </translation>
    </message>
    <message>
        <source> Copyright (C) 2018 Ingemar Ceicer. This is free software; see the GNU General Public Licence for copying conditions. There is NO warranty.
Least Common Denominator (LCD)</source>
        <translation type="vanished"> Upphovsrätt (C) 2018 Ingemar Ceicer. Detta är fri programvara; se GNU General Public License för kopieringsvillkor. Det finns ingen garanti.
Minsta Gemensamma Nämnare (MGN)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="86"/>
        <source> Copyright (C) 2018 </source>
        <translation> Copyright (C) 2018 </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="86"/>
        <source> Ingemar Ceicer. This is free software; see the GNU General Public Licence for copying conditions. There is NO warranty.
Least Common Denominator (LCD)</source>
        <translation> Ingemar Ceicer. Detta är fri programvara; se GNU General Public License för kopieringsvillkor. Det finns ingen garanti.
Minsta Gemensamma nämnare (MGN)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="156"/>
        <source>LCD = </source>
        <translation>MGN = </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="157"/>
        <source>Adds the fractional numbers
</source>
        <translation>Adderar bråktalen
</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="182"/>
        <location filename="mainwindow.cpp" line="205"/>
        <location filename="mainwindow.cpp" line="207"/>
        <location filename="mainwindow.cpp" line="213"/>
        <location filename="mainwindow.cpp" line="215"/>
        <source>Sum = </source>
        <translation>Summa = </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="194"/>
        <source>Shortens... divided by </source>
        <translation>Förkortar... dividerar med </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="194"/>
        <source> gets... </source>
        <translation> ger... </translation>
    </message>
    <message>
        <location filename="translate.cpp" line="30"/>
        <location filename="translate.cpp" line="55"/>
        <source>The program must be restarted for the new language settings to take effect.</source>
        <translation>Programet måste startas om för att de nya språkinställningarna skall gälla.</translation>
    </message>
    <message>
        <location filename="translate.cpp" line="31"/>
        <location filename="translate.cpp" line="56"/>
        <source>Restart Now</source>
        <translation>Starta om nu</translation>
    </message>
    <message>
        <location filename="translate.cpp" line="32"/>
        <location filename="translate.cpp" line="57"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="system.cpp" line="23"/>
        <source>This program uses Qt version </source>
        <translation>Detta program använder Qt version </translation>
    </message>
    <message>
        <location filename="system.cpp" line="23"/>
        <source> running on </source>
        <translation> körs på </translation>
    </message>
    <message>
        <location filename="system.cpp" line="25"/>
        <source> was created </source>
        <translation> skapades </translation>
    </message>
    <message>
        <location filename="system.cpp" line="25"/>
        <source>by a computer with</source>
        <translation>av en dator med</translation>
    </message>
    <message>
        <location filename="system.cpp" line="34"/>
        <location filename="system.cpp" line="36"/>
        <location filename="system.cpp" line="86"/>
        <source>Compiled by</source>
        <translation>Kompilerad med</translation>
    </message>
    <message>
        <location filename="system.cpp" line="42"/>
        <source>Full version number </source>
        <translation>Fullständigt versionsnummer </translation>
    </message>
    <message>
        <location filename="system.cpp" line="82"/>
        <source>Unknown version</source>
        <translation>Okänd version</translation>
    </message>
    <message>
        <location filename="system.cpp" line="88"/>
        <source>Unknown compiler.</source>
        <translation>Okänd komilator.</translation>
    </message>
</context>
</TS>
