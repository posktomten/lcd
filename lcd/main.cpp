/*
    LCD 1.0.2
    Copyright (C) 2018 - 2019 Ingemar Ceicer
    http://ceicer.org/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

/* In mathematics, the lowest common denominator or least
 * common denominator (abbreviated LCD) is the
 * lowest common multiple of the denominators of a set of fractions.
 * It simplifies adding, subtracting, and comparing fractions.
 */
#include <iostream>
//#include <ctype.h>
//#include <stdio.h>
using namespace std;
int getOnlyNumbers(int);
void lcd(long long[], long long[], long long);
void shortenFractionalNumbers(long long numerator, long long denominator);

#define MAX_FACTOR 100000000
int main()
{
    char fortsatta = 'n';

    do {
        cout << "LCD 1.0.2 Copyright (C) 2018 - 2019 Ingemar Ceicer.\n";
        cout << "This is free software; see the GNU General Public Licence\n";
        cout << "for copying conditions. There is NO warranty.\n";
        cout << "Least Common Denominator (LCD)\n";
        cout << "Enter the number of fractional numbers :>";
        /* cout << "int " << sizeof(int) << endl;
         cout << "long " << sizeof(long) << endl;
         cout << "long long " << sizeof(long long) << endl;
         cout << "unsigned long long " << sizeof(unsigned long long) << endl;*/
        long long counter = 0, i = 0, number = 0, numerator, denominator;
        cin >> number;

        if(number < 1)
            return 0;

        long long *numeratorArray = new long long[number];
        long long *denominatorArray = new long long[number];

        while(counter < number) {
            cout << "Fraction " << ++counter << ": numerator :>";
            cin >> numerator;
            numeratorArray[i] = numerator;
            cout << "Fraction " << counter << ": denominator :>";
            cin >> denominator;
            denominatorArray[i] = denominator;
            i++;
        }

        cout << endl;
        lcd(numeratorArray, denominatorArray, counter);
        delete[] numeratorArray;
        delete[] denominatorArray;
        cout << "\n\nPress Any Key To Exit, Press \'j\' To Continue :>";
        cin >> fortsatta;
#if defined _WIN32
        system("cls");
#endif
#if defined __linux__
        //cout << "\n\n\n\n";
        //printf("\x1B[2J");
        system("clear");
#endif
    } while((fortsatta == 'j') || (fortsatta == 'J'));

    return 0;
}
void lcd(long long numeratorArray[], long long denominatorArray[], long long counter)
{
    // Find the fractional number with the largest denominator
    long long factor, highest = 0, found, lcd, summa;
    long long *numerator = new long long[counter];
    // Practical test
    // double a1, b1, a2, b2;

    for(long long i = 0; i < counter; i++) {
        if(denominatorArray[i] > highest) {
            highest = denominatorArray[i];
        }
    }

    for(factor = 1; factor < MAX_FACTOR; factor++) {
        found = 0;

        for(long long i = 0; i < counter; i++) {
            if((((highest * factor)  %  denominatorArray[i]) == 0)) {
                lcd = highest * factor;
                numerator[i] =  numeratorArray[i] * (lcd / denominatorArray[i]);
                found++;
            } else
                break;
        }

        // If the LCD is found
        if(found == counter) {
            summa = 0;

            for(long long i = 0; i < counter; i++) {
                summa += numerator[i];
                cout << numeratorArray[i] << '/' << denominatorArray[i] << " = " << numerator[i] << '/' << lcd << endl;
                // Practical test
                /*
                a1=(double)numeratorArray[i];
                b1=(double)denominatorArray[i];
                a2=(double)numerator[i];
                b2=(double)lcd;
                cout << a1/b1 << '=' << a2/b2 << endl;
                */
            }

            cout << "LCD = " << lcd << endl;

            for(int i = 0; i < counter; i++) {
                if((i + 1) < counter)
                    cout << numeratorArray[i] << '/' << denominatorArray[i] << " + ";
                else
                    cout << numeratorArray[i] << '/' << denominatorArray[i];
            }

            cout << " = " << summa << '/' << lcd << endl;
            shortenFractionalNumbers(summa, lcd);
            delete[] numerator;
            return;
        }
    }
}
void shortenFractionalNumbers(long long numerator, long long denominator)
{
    if((numerator %  denominator) == 0) {
        cout << "Sum = " << numerator / denominator;
        return;
    }

    cout << "Trying to shorten... " << numerator << '/' << denominator << endl;
    //const long long hogst = numerator > denominator ? numerator : denominator;
    const long long lagst = numerator < denominator ? numerator : denominator;
    long long num = 0, den = 0, heltal = 0, del = 0;

    for(long long i = 2; i <= lagst; i++) {
        if(((numerator % i) == 0) && (((denominator % i) == 0))) {
            num = numerator / i;
            den = denominator / i;
            cout << "Shortens... divided by "  << i << " gets... " << num << '/' << den << endl;
        }
    }

    if(num == 0 && den == 0) {
        heltal = numerator / denominator;
        del = numerator % denominator;

        if(heltal > 0)
            cout << "Sum = " << numerator << '/' << denominator << " = " << heltal << " + " << del << '/' << denominator << endl;
        else
            cout << "Sum = " << numerator << '/' << denominator << endl;
    } else {
        heltal = num / den;
        del = num % den;

        if(heltal > 0)
            cout  << "Sum = " << numerator << '/' << denominator << " = " << num << '/' << den << " = " << heltal << " + " << del << '/' << den << endl;
        else
            cout << "Sum = " << numerator << '/' << denominator << " = " << num << '/' << den << endl;
    }
}
int getOnlyNumbers(int ch)
{
    int num = 0;

    if(isdigit(ch)) {
        return num;
    }

    return num;
}
