#ifndef CHECKFORUPDATES_H
#define CHECKFORUPDATES_H

#include <QObject>
#include <QNetworkReply>
#include <QNetworkAccessManager>

#define VERSION_PATH "http://bin.ceicer.com/lcd/version.txt"
#define SOURCE_LINK "https://gitlab.com/posktomten/lcd"
#define PROG_NAME "LCD"
#define VERSION "1.0.2"
class CheckForUpdates : public QObject
{
    Q_OBJECT

private:

    QString applicationHomepage;
    QNetworkReply  *reply;
    int jfrVersion(const QString *);

private slots:


    void checkForUpdates(QNetworkReply*);




public:
    void check();

    ~CheckForUpdates();

};

#endif // CHECKFORUPDATES_H
