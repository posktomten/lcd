/*
    lcd
    Copyright (C) 2018 Ingemar Ceicer
    http://ceicer.org/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/


#include "checkforupdates.h"
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QString>
#include <string>
#include <QObject>
#include <QDebug>
#include <iostream>

void CheckForUpdates::check()
{
    std::cout << QObject::tr("Trying to connect to server...").toStdString() << "\n";
    QUrl url(VERSION_PATH);
    auto *nam = new QNetworkAccessManager(this);
    reply = nam->get(QNetworkRequest(url));
    QObject::connect(nam, SIGNAL(finished(QNetworkReply*)), this, SLOT(checkForUpdates(QNetworkReply*)));
}

void CheckForUpdates::checkForUpdates(QNetworkReply *reply)
{
    QByteArray bytes = reply->readAll();  // bytes
    QString version(bytes); // string
    version = version.trimmed();
    // std::string version = v.toStdString();
    applicationHomepage = SOURCE_LINK;

    if(version == "") {
        std::cout << QObject::tr("No Internet connection was found.\nPlease check your Internet settings and firewall.").toStdString();
        //qInfo() << "No Internet connection was found. Please check your Internet settings and firewall.";
    } else {
        int ver = jfrVersion(&version);

        switch(ver) {
            case 0:
                std::cout << (QObject::tr("\nThere is a later version of ") + PROG_NAME + QObject::tr("Please download from") + "<p><a href=\"" + applicationHomepage + "\">" + applicationHomepage + "</a></p><p>" + QObject::tr("Latest version: ") + version + "</p>").toStdString();
                //qInfo() << QObject::tr("There is a later version of ") + tr(PROG_NAME) + QObject::tr(". Please download from ") + applicationHomepage + QObject::tr(". Latest version: ") + version;
                break;

            case 1:
                std::cout << (QObject::tr("\nYour version of ") + PROG_NAME + QObject::tr(" is later than the latest official version ") + "(" + version + ").").toStdString();
                //qInfo() << QObject::tr("Your version of ") + tr(PROG_NAME) + QObject::tr(" is later than the latest official version ") + "(" + version + ").";
                break;

            case 2:
                std::cout << QObject::tr("You have the latest version of ").toStdString() + PROG_NAME + ".";
                // qInfo() << QObject::tr("You have the latest version of ") + tr(PROG_NAME) + ".";
                break;

            default:
                std::cout << QObject::tr("\nThere was an error when the version was checked.").toStdString();
                //qInfo() << QObject::tr("There was an error when the version was checked.");
                break;
        };
    }

    return;
    // delete this;
}



int CheckForUpdates::jfrVersion(const QString *version)
{
    int v, tv;
    bool ok;
    QString this_version(VERSION);
    QStringList l_this_version = this_version.split(".");
    QStringList l_version = version->split(".");

    for(int i = 0; i <= 2; i++) {
        v = l_version[i].toInt(&ok, 10);
        tv = l_this_version[i].toInt(&ok, 10);

        if(!ok)
            return 3;

        if(v > tv)
            return 0;

        if(v < tv)
            return 1;
    }

    return 2;
}


CheckForUpdates::~CheckForUpdates()
{
    delete this;
}
