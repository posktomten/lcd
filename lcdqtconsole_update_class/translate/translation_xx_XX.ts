<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>QObject</name>
    <message>
        <location filename="checkforupdates.cpp" line="30"/>
        <source>Trying to connect to server...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="checkforupdates.cpp" line="46"/>
        <source>No Internet connection was found.
Please check your Internet settings and firewall.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="checkforupdates.cpp" line="53"/>
        <source>
There is a later version of </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="checkforupdates.cpp" line="53"/>
        <source>Please download from</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="checkforupdates.cpp" line="53"/>
        <source>Latest version: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="checkforupdates.cpp" line="58"/>
        <source> is later than the latest official version </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="checkforupdates.cpp" line="58"/>
        <source>
Your version of </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="checkforupdates.cpp" line="63"/>
        <source>You have the latest version of </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="checkforupdates.cpp" line="68"/>
        <source>
There was an error when the version was checked.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.cpp" line="78"/>
        <source>LCD </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.cpp" line="78"/>
        <source> Copyright (C) 2018 - 2019 Ingemar Ceicer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.cpp" line="79"/>
        <source>This is free software; see the GNU General Public Licence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.cpp" line="80"/>
        <source>for copying conditions. There is NO warranty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.cpp" line="81"/>
        <source>Least Common Denominator (LCD)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.cpp" line="83"/>
        <source>Would you like to check if there is an updated version? (y/N)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.cpp" line="100"/>
        <source>Enter the number of fractional numbers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.cpp" line="112"/>
        <location filename="main.cpp" line="115"/>
        <source>Fraction </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.cpp" line="112"/>
        <source>: numerator :&gt; </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.cpp" line="115"/>
        <source>: denominator :&gt; </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.cpp" line="173"/>
        <source>LCD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.cpp" line="192"/>
        <location filename="main.cpp" line="213"/>
        <location filename="main.cpp" line="215"/>
        <location filename="main.cpp" line="221"/>
        <location filename="main.cpp" line="223"/>
        <source>Sum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.cpp" line="204"/>
        <source>Shortens... divided by </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.cpp" line="204"/>
        <source> gets... </source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
