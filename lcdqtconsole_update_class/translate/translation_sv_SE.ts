<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>QObject</name>
    <message>
        <location filename="checkforupdates.cpp" line="30"/>
        <source>Trying to connect to server...</source>
        <translation>Försöker ansluta till servern...</translation>
    </message>
    <message>
        <source>There is a later version of </source>
        <translation type="vanished">Det finns en senare vrsion av </translation>
    </message>
    <message>
        <source>. Please download from </source>
        <translation type="vanished">. Vänligen ladda ner från </translation>
    </message>
    <message>
        <source>. Latest version: </source>
        <translation type="vanished">. Senaste version: </translation>
    </message>
    <message>
        <source>Your version of </source>
        <translation type="vanished">Din version av </translation>
    </message>
    <message>
        <location filename="checkforupdates.cpp" line="46"/>
        <source>No Internet connection was found.
Please check your Internet settings and firewall.</source>
        <translation>Ingen internetanslutning hittades.
Kontrollera din internettanslutning och din brandvägg.</translation>
    </message>
    <message>
        <location filename="checkforupdates.cpp" line="53"/>
        <source>
There is a later version of </source>
        <translation>
Det finns en senare version av </translation>
    </message>
    <message>
        <location filename="checkforupdates.cpp" line="53"/>
        <source>Please download from</source>
        <translation>Vänligen ladda ner från</translation>
    </message>
    <message>
        <location filename="checkforupdates.cpp" line="53"/>
        <source>Latest version: </source>
        <translation>Senaste versionen: </translation>
    </message>
    <message>
        <location filename="checkforupdates.cpp" line="58"/>
        <source> is later than the latest official version </source>
        <translation> är nyare än den senaste officiella versionen </translation>
    </message>
    <message>
        <location filename="checkforupdates.cpp" line="58"/>
        <source>
Your version of </source>
        <translation>
Din verssion av </translation>
    </message>
    <message>
        <location filename="checkforupdates.cpp" line="63"/>
        <source>You have the latest version of </source>
        <translation>Du har den senaste versionen av </translation>
    </message>
    <message>
        <location filename="checkforupdates.cpp" line="68"/>
        <source>
There was an error when the version was checked.</source>
        <translation>
Det inträffade ett fel när versionen kontrollerades.</translation>
    </message>
    <message>
        <source>There was an error when the version was checked.</source>
        <translation type="vanished">Det inträffade ett fel när versionen kontrollerades.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="78"/>
        <source>LCD </source>
        <translation>MGN </translation>
    </message>
    <message>
        <location filename="main.cpp" line="78"/>
        <source> Copyright (C) 2018 - 2019 Ingemar Ceicer.</source>
        <translation> Upphovsrätt (C) 2018-2019 Ingemar Ceicer.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="79"/>
        <source>This is free software; see the GNU General Public Licence</source>
        <translation>Detta är fri mjukvara; se GNU General Public Licence</translation>
    </message>
    <message>
        <location filename="main.cpp" line="80"/>
        <source>for copying conditions. There is NO warranty.</source>
        <translation>för kopieringsvilkor. Det finns INGA garantier.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="81"/>
        <source>Least Common Denominator (LCD)</source>
        <translation>Minsta Gemensamma Nämnare (MGN)</translation>
    </message>
    <message>
        <location filename="main.cpp" line="83"/>
        <source>Would you like to check if there is an updated version? (y/N)</source>
        <translation>Vill du kolla om det finns en uppdaterad version? (y/N)</translation>
    </message>
    <message>
        <location filename="main.cpp" line="100"/>
        <source>Enter the number of fractional numbers</source>
        <translation>Ange antalet bråktal</translation>
    </message>
    <message>
        <location filename="main.cpp" line="112"/>
        <location filename="main.cpp" line="115"/>
        <source>Fraction </source>
        <translation>Bråktal </translation>
    </message>
    <message>
        <location filename="main.cpp" line="112"/>
        <source>: numerator :&gt; </source>
        <translation>: täljare:&gt; </translation>
    </message>
    <message>
        <location filename="main.cpp" line="115"/>
        <source>: denominator :&gt; </source>
        <translation>: nämnare :&gt; </translation>
    </message>
    <message>
        <location filename="main.cpp" line="173"/>
        <source>LCD</source>
        <translation>MGN</translation>
    </message>
    <message>
        <location filename="main.cpp" line="192"/>
        <location filename="main.cpp" line="213"/>
        <location filename="main.cpp" line="215"/>
        <location filename="main.cpp" line="221"/>
        <location filename="main.cpp" line="223"/>
        <source>Sum</source>
        <translation>Summa</translation>
    </message>
    <message>
        <location filename="main.cpp" line="204"/>
        <source>Shortens... divided by </source>
        <translation>Förkortar... deviderar med </translation>
    </message>
    <message>
        <location filename="main.cpp" line="204"/>
        <source> gets... </source>
        <translation> ger... </translation>
    </message>
</context>
</TS>
