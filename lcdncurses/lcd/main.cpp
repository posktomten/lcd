/*
    LCD
    Copyright (C) 2018 - 2019 Ingemar Ceicer
    http://ceicer.org/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

/* In mathematics, the lowest common denominator or least
 * common denominator (abbreviated LCD) is the
 * lowest common multiple of the denominators of a set of fractions.
 * It simplifies adding, subtracting, and comparing fractions.
 */
/*
 2/5 = 126/315
 6/9 = 210/315
 9/3 = 945/315
 1/7 = 45/315
 3/5 = 189/315
 LCD = 315
 2/5 +  6/9 +  9/3 +  1/7 + 3/5 = 1515/315
 Trying to shorten... 1515/315
 Shortens... divided by 3 gets... 505/105
 Shortens... divided by 5 gets... 303/63
 Shortens... divided by 15 gets... 101/21
 Sum = 1515/315 = 101/21 = 4 + 17/21
 */

/*
      Configuration summary for NCURSES 6.1 20180127:

      extended funcs: yes
      xterm terminfo: xterm-new
      bin directory: /opt/ncurses/bin
      lib directory: /opt/ncurses/lib
      include directory: /opt/ncurses/include/ncurses
      man directory: /opt/ncurses/share/man
      terminfo directory: /opt/ncurses/share/terminfo
      *
      Include-directory is not in a standard location
      *
 * alias m='g++ -L /opt/ncurses/lib -I /opt/ncurses/include -o test main.cpp -lncurses'
 * alias n='clang++ -L /opt/ncurses/lib -I /opt/ncurses/include -o test main.cpp -lncurses'
 * g++ -L /opt/ncurses/lib -I /opt/ncurses/include -o main main.cpp -lncurses
 *
 * Geany Build command:
 * g++ -Wall -o "%e" "%f"  -L /opt/ncurses/lib -I /opt/ncurses/include -lncurses
 * clang++ -Wall -o "%e" "%f"  -L /opt/ncurses/lib -I /opt/ncurses/include -lncurses
*/
/*
 * http://tldp.org/HOWTO/NCURSES-Programming-HOWTO/keys.html
 */

#include <ncurses/ncurses.h>  // OBS!
#include <ctype.h>
#define MAX_FACTOR 100000000
//using namespace std;
int getOnlyNumbers(char ch);
void lcd(long long[], long long[], long long);
void shortenFractionalNumbers(long long numerator, long long denominator);


int main(int argc, char *argv[])
{

    initscr();          /* Start curses mode        */
    //cbreak();           /* Line buffering disabled, Pass on */
    /* everty thing to me       */
    keypad(stdscr, TRUE);       /* I need that nifty F1     */
    //printw("Press F1 to exit");
    //refresh();
    curs_set(0);
    char fortsatta = 'n';

    do {
        clear();
        attrset(A_ITALIC);
        mvprintw(1, 2, "LCD 1.0.2 Copyright (C) 2018 - 2019 Ingemar Ceicer.\n");
        mvprintw(2, 2, "This is free software; see the GNU General Public Licence\n");
        mvprintw(3, 2, "for copying conditions. There is NO warranty.\n");
        mvprintw(4, 2, "Least Common Denominator (LCD)\n");
        attrset(A_NORMAL);
        mvprintw(6, 2, "Enter the number of fractional numbers");
        refresh();
        /* cout << "int " << sizeof(int) << endl;
         cout << "long " << sizeof(long) << endl;
         cout << "long long " << sizeof(long long) << endl;
         cout << "unsigned long long " << sizeof(unsigned long long) << endl;*/
        long long counter = 0, i = 0, number, numerator, denominator;
        char ch;
        ch = getch();
        number = getOnlyNumbers(ch);

        while(number == 0) {
            clear();

            if(has_colors()) {
                use_default_colors();
                start_color();
                init_pair(2, COLOR_RED, -1);
                attrset(COLOR_PAIR(2) | A_BOLD);
            }

            mvprintw(1, 2, "Please select a digit, separated from 0");
            ch = getch();
            number = getOnlyNumbers(ch);
        }

        if(has_colors()) {
            use_default_colors();
            start_color();
            init_pair(1, -1, -1);
            attrset(COLOR_PAIR(1));
        }

        clear();
        mvprintw(1, 2, "You chose %i fractional numbers", number);
        refresh();
        // getch();
        // if(number < 1)
        //    return 0;
        long long *numeratorArray = new long long[number];
        long long *denominatorArray = new long long[number];
        //clear();
        int row = 0;

        while(counter < number) {
            counter++;
            mvprintw(2, 2, "                                                                        ");
            refresh();
            mvprintw(2, 2, "Fraction  %i : numerator", counter);
            refresh();
            ch = getch();
            numerator = getOnlyNumbers(ch);

            while(numerator == 0) {
                // clear();
                if(has_colors()) {
                    use_default_colors();
                    start_color();
                    init_pair(2, COLOR_RED, -1);
                    attrset(COLOR_PAIR(2) | A_BOLD);
                }

                mvprintw(2, 2, "                                                                        ");
                mvprintw(2, 2, "Please select a digit, separated from 0");
                refresh();
                ch = getch();
                numerator = getOnlyNumbers(ch);
            }

            if(has_colors()) {
                use_default_colors();
                start_color();
                init_pair(1, -1, -1);
                attrset(COLOR_PAIR(1));
            }

            mvprintw(row + 3, 2, "Fraction  %i: %i/x", counter, numerator);
            refresh();
            numeratorArray[i] = numerator;
            //
            mvprintw(2, 2, "                                                                          ");
            mvprintw(2, 2, "Fraction  %i : denominator", counter);
            refresh();
            ch = getch();
            denominator = getOnlyNumbers(ch);

            while(denominator == 0) {
                // clear();
                if(has_colors()) {
                    use_default_colors();
                    start_color();
                    init_pair(2, COLOR_RED, -1);
                    attrset(COLOR_PAIR(2) | A_BOLD);
                }

                mvprintw(2, 2, "                                                                          ");
                refresh();
                mvprintw(2, 2, "Please select a digit, separated from 0");
                ch = getch();
                denominator = getOnlyNumbers(ch);
            }

            if(has_colors()) {
                use_default_colors();
                start_color();
                init_pair(1, -1, -1);
                attrset(COLOR_PAIR(1));
            }

            mvprintw(row + 3, 2, "Fraction  %i: %i/%i", counter, numerator, denominator);
            refresh();
            denominatorArray[i] = denominator;
            i++;
            row++;
        }

        clear();
        lcd(numeratorArray, denominatorArray, counter);
        delete[] numeratorArray;
        delete[] denominatorArray;

        // system("pause");
        //return 0;
        if(has_colors()) {
            use_default_colors();
            start_color();
            init_pair(2, COLOR_RED, -1);
            attrset(COLOR_PAIR(2) | A_BOLD);
        }

      //  mvprintw(row + 6, 2, "Press Any Key To Exit, Press \'j\' To Continue"
    } while((fortsatta == 'j') || (fortsatta == 'J'));

    // clear();
   // endwin();

}

void lcd(long long numeratorArray[], long long denominatorArray[], long long counter)
{
    printw("\n");
    // Find the fractional number with the largest denominator
    long long factor, highest = 0, found, lcd, summa;
    long long *numerator = new long long[counter];
    // Practical test
    // double a1, b1, a2, b2;

    for(long long i = 0; i < counter; i++) {
        if(denominatorArray[i] > highest) {
            highest = denominatorArray[i];
        }
    }

    for(factor = 1; factor < MAX_FACTOR; factor++) {
        found = 0;

        for(long long i = 0; i < counter; i++) {
            if((((highest * factor)  %  denominatorArray[i]) == 0)) {
                lcd = highest * factor;
                numerator[i] =  numeratorArray[i] * (lcd / denominatorArray[i]);
                found++;
            } else
                break;
        }

        // If the LCD is found
        if(found == counter) {
            summa = 0;

            for(long long i = 0; i < counter; i++) {
                summa += numerator[i];
                printw("  %i/%i = %i/%i\n", numeratorArray[i], denominatorArray[i], numerator[i], lcd);
                refresh();
                // Practical test
                /*
                a1=(double)numeratorArray[i];
                b1=(double)denominatorArray[i];
                a2=(double)numerator[i];
                b2=(double)lcd;
                cout << a1/b1 << '=' << a2/b2 << endl;
                */
            }

            printw("  LCD = %i\n", lcd);
            refresh();

            for(int i = 0; i < counter; i++) {
                if((i + 1) < counter) {
                    printw("  %i/%i +", numeratorArray[i], denominatorArray[i]);
                    refresh();
                } else {
                    printw("  %i/%i", numeratorArray[i], denominatorArray[i]);
                    refresh();
                }
            }

            printw(" = %i/%i\n", summa, lcd);
            shortenFractionalNumbers(summa, lcd);
            delete[] numerator;
            return;
        }
    }
}
void shortenFractionalNumbers(long long numerator, long long denominator)
{
    if((numerator %  denominator) == 0) {
        int s = numerator / denominator;
        printw("  Sum = %i", s);
        refresh();
        return;
    }

    printw("  Trying to shorten... %i/%i\n", numerator, denominator);
    refresh();
    //const long long hogst = numerator > denominator ? numerator : denominator;
    const long long lagst = numerator < denominator ? numerator : denominator;
    long long num = 0, den = 0, heltal = 0, del = 0;

    for(long long i = 2; i <= lagst; i++) {
        if(((numerator % i) == 0) && (((denominator % i) == 0))) {
            num = numerator / i;
            den = denominator / i;
            printw("  Shortens... divided by %i gets... %i/%i\n", i, num, den);
            refresh();
        }
    }

    if(num == 0 && den == 0) {
        heltal = numerator / denominator;
        del = numerator % denominator;

        if(heltal > 0) {
            printw("  Sum = %i/%i = %i + %i/%i\n", numerator, denominator, heltal, del, denominator);
            refresh();
        } else {
            printw("  Sum = %i/%i\n", numerator, denominator);
            refresh();
        }
    } else {
        heltal = num / den;
        del = num % den;

        if(heltal > 0) {
            printw("  Sum = %i/%i = %i/%i = %i + %i/%i\n", numerator, denominator, num, den, heltal, del, den);
            refresh();
        } else {
            printw("  Sum = %i/%i = %i/%i\n", numerator, denominator, num, den);
            refresh();
        }
    }
}
int getOnlyNumbers(char ch)
{
    int num = 0;

    if(isdigit(ch)) {
        num = (ch - 48);
        return num;
    }

    return num;
}
