/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.15.12
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionAdd;
    QAction *actionLcd;
    QAction *actionExit;
    QAction *actionClear;
    QAction *actionEnglish;
    QAction *actionSwedish;
    QAction *actionCheckForUpdates;
    QAction *actionAbout;
    QAction *actionLicense;
    QAction *actionCheckForSoftwareUpdatesAtProgramStartup;
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout_3;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout_2;
    QSpinBox *sbNumerator;
    QFrame *line;
    QSpinBox *sbDenominator;
    QPushButton *pbAdd;
    QPushButton *pbLcd;
    QPushButton *pbClear;
    QSpacerItem *horizontalSpacer_2;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout;
    QPlainTextEdit *pteFraction;
    QMenuBar *menuBar;
    QMenu *menuFie;
    QMenu *menuLanguage;
    QMenu *menuHelp;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(406, 532);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/images/fraction.png"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        actionAdd = new QAction(MainWindow);
        actionAdd->setObjectName(QString::fromUtf8("actionAdd"));
        actionLcd = new QAction(MainWindow);
        actionLcd->setObjectName(QString::fromUtf8("actionLcd"));
        actionExit = new QAction(MainWindow);
        actionExit->setObjectName(QString::fromUtf8("actionExit"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/exit.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionExit->setIcon(icon1);
        actionClear = new QAction(MainWindow);
        actionClear->setObjectName(QString::fromUtf8("actionClear"));
        actionEnglish = new QAction(MainWindow);
        actionEnglish->setObjectName(QString::fromUtf8("actionEnglish"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/images/english.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionEnglish->setIcon(icon2);
        actionSwedish = new QAction(MainWindow);
        actionSwedish->setObjectName(QString::fromUtf8("actionSwedish"));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/images/swedish.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionSwedish->setIcon(icon3);
        actionCheckForUpdates = new QAction(MainWindow);
        actionCheckForUpdates->setObjectName(QString::fromUtf8("actionCheckForUpdates"));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/images/update.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionCheckForUpdates->setIcon(icon4);
        actionAbout = new QAction(MainWindow);
        actionAbout->setObjectName(QString::fromUtf8("actionAbout"));
        QIcon icon5;
        icon5.addFile(QString::fromUtf8(":/images/about.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionAbout->setIcon(icon5);
        actionLicense = new QAction(MainWindow);
        actionLicense->setObjectName(QString::fromUtf8("actionLicense"));
        QIcon icon6;
        icon6.addFile(QString::fromUtf8(":/images/gpl.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionLicense->setIcon(icon6);
        actionCheckForSoftwareUpdatesAtProgramStartup = new QAction(MainWindow);
        actionCheckForSoftwareUpdatesAtProgramStartup->setObjectName(QString::fromUtf8("actionCheckForSoftwareUpdatesAtProgramStartup"));
        actionCheckForSoftwareUpdatesAtProgramStartup->setCheckable(true);
        actionCheckForSoftwareUpdatesAtProgramStartup->setEnabled(true);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        horizontalLayout_3 = new QHBoxLayout(centralWidget);
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_3->setSizeConstraint(QLayout::SetMaximumSize);
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        sbNumerator = new QSpinBox(centralWidget);
        sbNumerator->setObjectName(QString::fromUtf8("sbNumerator"));
        sbNumerator->setMinimum(-2147483647);
        sbNumerator->setMaximum(2147483647);
        sbNumerator->setValue(1);

        verticalLayout_2->addWidget(sbNumerator);

        line = new QFrame(centralWidget);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShadow(QFrame::Plain);
        line->setLineWidth(4);
        line->setMidLineWidth(0);
        line->setFrameShape(QFrame::HLine);

        verticalLayout_2->addWidget(line);

        sbDenominator = new QSpinBox(centralWidget);
        sbDenominator->setObjectName(QString::fromUtf8("sbDenominator"));
        sbDenominator->setMinimum(-2147483647);
        sbDenominator->setMaximum(2147483647);
        sbDenominator->setSingleStep(1);
        sbDenominator->setValue(1);

        verticalLayout_2->addWidget(sbDenominator);


        horizontalLayout_2->addLayout(verticalLayout_2);

        pbAdd = new QPushButton(centralWidget);
        pbAdd->setObjectName(QString::fromUtf8("pbAdd"));

        horizontalLayout_2->addWidget(pbAdd);

        pbLcd = new QPushButton(centralWidget);
        pbLcd->setObjectName(QString::fromUtf8("pbLcd"));

        horizontalLayout_2->addWidget(pbLcd);

        pbClear = new QPushButton(centralWidget);
        pbClear->setObjectName(QString::fromUtf8("pbClear"));

        horizontalLayout_2->addWidget(pbClear);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);


        verticalLayout_3->addLayout(horizontalLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        pteFraction = new QPlainTextEdit(centralWidget);
        pteFraction->setObjectName(QString::fromUtf8("pteFraction"));
        QFont font;
        font.setPointSize(11);
        pteFraction->setFont(font);
        pteFraction->setAcceptDrops(false);
        pteFraction->setInputMethodHints(Qt::ImhMultiLine);
        pteFraction->setFrameShape(QFrame::StyledPanel);
        pteFraction->setFrameShadow(QFrame::Plain);
        pteFraction->setLineWidth(1);
        pteFraction->setUndoRedoEnabled(false);
        pteFraction->setCursorWidth(0);
        pteFraction->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);
        pteFraction->setBackgroundVisible(false);

        verticalLayout->addWidget(pteFraction);


        horizontalLayout->addLayout(verticalLayout);


        verticalLayout_3->addLayout(horizontalLayout);


        horizontalLayout_3->addLayout(verticalLayout_3);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 406, 28));
        menuBar->setFont(font);
        menuFie = new QMenu(menuBar);
        menuFie->setObjectName(QString::fromUtf8("menuFie"));
        menuFie->setTearOffEnabled(false);
        menuLanguage = new QMenu(menuBar);
        menuLanguage->setObjectName(QString::fromUtf8("menuLanguage"));
        menuHelp = new QMenu(menuBar);
        menuHelp->setObjectName(QString::fromUtf8("menuHelp"));
        MainWindow->setMenuBar(menuBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuFie->menuAction());
        menuBar->addAction(menuLanguage->menuAction());
        menuBar->addAction(menuHelp->menuAction());
        menuFie->addAction(actionAdd);
        menuFie->addAction(actionLcd);
        menuFie->addAction(actionClear);
        menuFie->addAction(actionExit);
        menuLanguage->addAction(actionEnglish);
        menuLanguage->addAction(actionSwedish);
        menuHelp->addAction(actionCheckForUpdates);
        menuHelp->addAction(actionAbout);
        menuHelp->addAction(actionLicense);
        menuHelp->addAction(actionCheckForSoftwareUpdatesAtProgramStartup);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        actionAdd->setText(QCoreApplication::translate("MainWindow", "Add", nullptr));
        actionLcd->setText(QCoreApplication::translate("MainWindow", "LCD", nullptr));
        actionExit->setText(QCoreApplication::translate("MainWindow", "Exit", nullptr));
        actionClear->setText(QCoreApplication::translate("MainWindow", "Clear", nullptr));
        actionEnglish->setText(QCoreApplication::translate("MainWindow", "English", nullptr));
        actionSwedish->setText(QCoreApplication::translate("MainWindow", "Swedish", nullptr));
        actionCheckForUpdates->setText(QCoreApplication::translate("MainWindow", "Check for updates", nullptr));
        actionAbout->setText(QCoreApplication::translate("MainWindow", "About", nullptr));
        actionLicense->setText(QCoreApplication::translate("MainWindow", "License", nullptr));
        actionCheckForSoftwareUpdatesAtProgramStartup->setText(QCoreApplication::translate("MainWindow", "Check for software updates at program startup", nullptr));
        pbAdd->setText(QCoreApplication::translate("MainWindow", "Add", nullptr));
        pbLcd->setText(QCoreApplication::translate("MainWindow", "LCD", nullptr));
        pbClear->setText(QCoreApplication::translate("MainWindow", "Clear", nullptr));
        menuFie->setTitle(QCoreApplication::translate("MainWindow", "&File", nullptr));
        menuLanguage->setTitle(QCoreApplication::translate("MainWindow", "&Language", nullptr));
        menuHelp->setTitle(QCoreApplication::translate("MainWindow", "&Help", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
