#-------------------------------------------------
#
# Project created by QtCreator 2018-10-14T00:19:05
#
#-------------------------------------------------

QT += core gui network concurrent

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = lcdqt
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0
#CONFIG += /opt/Qt5.14.1_static/bin/lrelease embed_translations
CONFIG += C:\Qt\Qt5.14.1\5.14.1\mingw73_64\bin\lrelease.exe embed_translations

#    linux-g++ | win32 {
#        CONFIG += precompile_header
#   }

CONFIG += c++11

SOURCES += \
        main.cpp \
        mainwindow.cpp \
        translate.cpp \
        system.cpp \
        checkforupdates.cpp

HEADERS += \
        mainwindow.h \
        checkforupdates.h

FORMS += \
        mainwindow.ui

unix: CONFIG  (release, debug|release): LIBS += -L../lib5/ -lcrypto # Release
unix: CONFIG  (release, debug|release): LIBS += -L../lib5/ -lssl # Release


# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
        resurs.qrc

TRANSLATIONS += \
        i18n/translation_sv_SE.ts \
        i18n/translation_xx_XX.ts

RC_FILE = myapp.rc
DESTDIR=../build-executable5
unix:UI_DIR = ../code
win32:UI_DIR = ../code
