/*
    lcd
    Copyright (C) 2018 - 2020 Ingemar Ceicer
    http://ceicer.org/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include "checkforupdates.h"

#include <QTranslator>
#include <QApplication>
#include <QMainWindow>
#include <QSettings>
#include <QMessageBox>
#include <QDir>
#include <QProcess>
#include <QScreen>
#include <QDebug>
#include <QFontDatabase>
#include <QtMath>
#include <QFuture>
#include <QtConcurrent>
#include <QThread>
#include <climits>
#include <limits.h>

#define CURRENT_YEAR "2020"
//#define MAX_FACTOR 100000
#define MAX_FACTOR  9223372036854775807 // 8 byte - 1
#define BUILD_DATE_TIME __DATE__ " " __TIME__
#define SOURCE_LINK "https://gitlab.com/posktomten/lcd"
#define HOME_LINK "https://gitlab.com/posktomten/lcd/-/wikis/home"
#define DOWNLOAD_PATH "https://gitlab.com/posktomten/lcd/-/wikis/Download/Version-1.0.4" // checkforupdates
#define VERSION_PATH "http://bin.ceicer.com/lcdqt/version2.txt" // checkforupdates
#define PROG_NAME "LCD"  // checkforupdates
#define VERSION "1.0.6"  // checkforupdates
#define LINUX_FONT 13
#define WINDOWS_FONT 11
namespace Ui
{
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();


private:
    Ui::MainWindow *ui;
    QVector<qint64> *numeratorArray;
    QVector<qint64> *denominatorArray;
    void setStartConfig();
    void setEndConfig();
    void shortenFractionalNumbers(qint64, qint64);
    void startaom();
    QString getSystem();
    bool om;
    bool gpl;


private slots:
    void add();
    void lcd();
    void clear();
    void english();
    void swedish();
    void about();
    void license();

};

#endif // MAINWINDOW_H
