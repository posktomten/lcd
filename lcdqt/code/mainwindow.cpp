/*
    lcd
    Copyright (C) 2018 - 2020 Ingemar Ceicer
    http://ceicer.org/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/
#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    this->setAttribute(Qt::WA_DeleteOnClose, true);
    ui->setupUi(this);
    setStartConfig();
    //qDebug() << sizeof(qint64);
    // ui->sbDenominator->setValidator(new QRegExpValidator(QRegExp(QString("^[1-9][0-9]*$"))));
    connect(ui->actionAdd, &QAction::triggered, [this] { add(); });
    connect(ui->pbAdd, &QPushButton::released, [this] { add(); });
    connect(ui->actionLcd, &QAction::triggered, [this] { lcd(); });
    connect(ui->pbLcd, &QPushButton::released, [this] { lcd(); });
    connect(ui->actionClear, &QAction::triggered, [this] { clear(); });
    connect(ui->pbClear, &QPushButton::released, [this] { clear(); });
    connect(ui->actionExit, &QAction::triggered, [this] { close(); });
    connect(ui->actionEnglish, &QAction::triggered, [this] { english(); });
    connect(ui->actionSwedish, &QAction::triggered, [this] { swedish(); });
    connect(ui->actionCheckForUpdates, &QAction::triggered, [] {
        CheckForUpdates *cu = new CheckForUpdates;
        cu->check(PROG_NAME, VERSION, VERSION_PATH, DOWNLOAD_PATH);
    });
    connect(ui->actionAbout, &QAction::triggered, [this] { about(); });
    connect(ui->actionLicense, &QAction::triggered, [this] { license(); });
    connect(ui->sbDenominator, QOverload<int>::of(&QSpinBox::valueChanged),
    [ this ](int i) {
        if(i == 0) {
            ui->sbDenominator->setToolTip(tr("Division by zero!"));
            ui->pbAdd->setToolTip(tr("Division by zero!"));
            ui->statusBar->showMessage(tr("Division by zero!"));
        } else {
            ui->sbDenominator->setToolTip("");
            ui->pbAdd->setToolTip(tr(""));
            ui->statusBar->showMessage("");
        }
    });
    connect(ui->actionCheckForSoftwareUpdatesAtProgramStartup, &QAction::triggered,
    [this]() {
        bool checkforupdatesatstartup;

        if(ui->actionCheckForSoftwareUpdatesAtProgramStartup->isChecked()) {
            checkforupdatesatstartup = true;
        } else {
            checkforupdatesatstartup = false;
        }

        QSettings settings(QSettings::IniFormat, QSettings::UserScope, PROG_NAME, PROG_NAME);
        settings.setIniCodec("UTF-8");
        settings.beginGroup("MainWindow");
        settings.setValue("checkforupdatesatstartup", checkforupdatesatstartup);
        settings.endGroup();
    });
    om = true;
    gpl = true;
    numeratorArray = new QVector<qint64>;
    denominatorArray = new QVector<qint64>;
}

MainWindow::~MainWindow()
{
    setEndConfig();
    delete ui;
}
void MainWindow::setStartConfig()
{
#ifdef Q_OS_LINUX     // Linux
    QFont f = QFont("PT Mono", LINUX_FONT, 1);
#endif
#ifdef Q_OS_WIN // Windows 32- and 64-bit
    QFont f = QFont("PT Mono", WINDOWS_FONT, 1);
#endif
    ui->menuBar->setFont(f);
    ui->pteFraction->setFont(f);
    QScreen *screen = QGuiApplication::primaryScreen();
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, PROG_NAME, PROG_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("MainWindow");
    this->resize(settings.value("size", QSize(200, 600)).toSize());
    QPoint p = settings.value("pos", QPoint(100, 100)).toPoint();
    QRect  screenGeometry = screen->virtualGeometry();
    QPoint screenSize(screenGeometry.width(), screenGeometry.height());
    QPoint  screenSavedSize = settings.value("screenSavedSize", QPoint(0, 0)).toPoint();
    bool checkforupdatesatstartup = settings.value("checkforupdatesatstartup", true).toBool();
    settings.endGroup();

    if(screenSavedSize == screenSize)
        this->move(p);
    else
        this->move(100, 100);

    if(checkforupdatesatstartup) {
        ui->actionCheckForSoftwareUpdatesAtProgramStartup->setChecked(true);
        CheckForUpdates *cu = new CheckForUpdates;
        cu->checkOnStart(PROG_NAME, VERSION, VERSION_PATH, DOWNLOAD_PATH);
    } else {
        ui->actionCheckForSoftwareUpdatesAtProgramStartup->setChecked(false);
    }

    this->setWindowTitle(PROG_NAME " " VERSION);
    // about();
}
void MainWindow::setEndConfig()
{
    QScreen *screen = QGuiApplication::primaryScreen();
    QRect  screenGeometry = screen->virtualGeometry();
    QPoint screenSize(screenGeometry.width(), screenGeometry.height());
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, PROG_NAME, PROG_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("MainWindow");
    settings.setValue("screenSavedSize", screenSize);
    settings.setValue("size", size());
    settings.setValue("pos", pos());
    settings.endGroup();
}
void MainWindow::about()
{
    if(om) {
        const QString intro = PROG_NAME " " VERSION + tr(" Copyright (C) 2018 ") + "- " + CURRENT_YEAR + tr(" Ingemar Ceicer.\nThis is free software; see the GNU General Public Licence\nfor copying conditions. There is NO warranty.\nLeast Common Denominator (LCD)") + "\n\n" + tr("Source code\n");
        const QString link = SOURCE_LINK;
        const QString homelink = HOME_LINK;
        ui->pteFraction->moveCursor(QTextCursor::Start);
        ui->pteFraction->insertPlainText(intro);
        ui->pteFraction->insertPlainText(link);
        ui->pteFraction->insertPlainText("\n" + tr("Homepage") + "\n");
        ui->pteFraction->insertPlainText(homelink);
        ui->pteFraction->insertPlainText("\n\n\n");
        const QString info = getSystem();
        ui->pteFraction->insertPlainText(info);
        ui->pteFraction->insertPlainText("\n");
        om = false;
    }
}

void MainWindow::license()
{
    if(gpl) {
        QFile file(":/txt/license.txt");
        QTextStream in(&file);

        if(!file.open(QFile::ReadOnly | QFile::Text))
            return;

        QString s = in.readAll();
        ui->pteFraction->moveCursor(QTextCursor::Start);
        ui->pteFraction->insertPlainText(s);
        ui->pteFraction->insertPlainText("\n\n");
    }
}

void MainWindow::clear()
{
    ui->pteFraction->clear();
    numeratorArray->clear();
    denominatorArray->clear();
    om = true;
    gpl = true;
}
void MainWindow::add()
{
    qint64 n = ui->sbNumerator->value();
    qint64 d = ui->sbDenominator->value();

    if(d == 0) {
        ui->sbDenominator->setToolTip(tr("Division by zero!"));
        ui->pbAdd->setToolTip(tr("Division by zero!"));
        ui->statusBar->showMessage(tr("Division by zero!"));
        return;
    } else {
        ui->sbDenominator->setToolTip(tr(""));
        ui->pbAdd->setToolTip(tr(""));
        ui->statusBar->showMessage(tr(""));
    }

    if(d == 0) {
        return;
    }

    if(d < 0) {
        n = n * (-1);
        d = d * (-1);
    }

    //    qDebug() << sizeof(qint64);
    //    qDebug() << sizeof(int);
    numeratorArray->push_back(n);
    denominatorArray->push_back(d);
    QString text = QString::number(n) + '/' + QString::number(d) + ' ' ;
    ui->pteFraction->moveCursor(QTextCursor::End);
    ui->pteFraction->appendPlainText(text);
}
void MainWindow::lcd()
{
    qint64  found = 0, lcd = 0, summa;
    qint64 counter = numeratorArray->size();
    qint64 highest = 0, factor;

    if(counter < 1)
        return;

    auto *numerator = new qint64[counter];
    // Find the fractional number with the largest denominator
    QVector<qint64>::iterator it;
    QVector<qint64>::iterator nit;

    for(it = denominatorArray->begin(); it < denominatorArray->end(); it++) {
        if(*it > highest) {
            highest = *it;
        }
    }

    for(factor = 1; factor < MAX_FACTOR; factor++) {
        found = 0;

        for(nit = numeratorArray->begin(), it = denominatorArray->begin(); it < denominatorArray->end(); it++, nit++) {
            if((((highest * factor)  %  *it) == 0)) {
                // ipac
                // if((highest > LLONG_MAX / factor) || (factor > LLONG_MAX / highest)) {
                if(highest > LLONG_MAX / factor) {
                    ui->pteFraction->appendPlainText(tr("Overflow (Denominator)"));
                    numeratorArray->pop_back();
                    denominatorArray->pop_back();
                    return;
                }

                lcd = highest * factor;
                numerator[found] = (*nit) * (lcd / *it);
                found++;
            } else
                break;
        }

        // If the LCD is found
        if(found == counter) {
            qint64 j = 0;
            summa = 0;

            for(nit = numeratorArray->begin(), it = denominatorArray->begin(); nit < numeratorArray->end(); nit++, it++) {
                summa += numerator[j];
                ui->pteFraction->appendPlainText(QString::number(*nit) + '/' + QString::number(*it) + " = " + QString::number(numerator[j++]) + '/' + QString::number(lcd));
            }

            ui->pteFraction->appendPlainText(tr("LCD = ") + QString::number(lcd));
            ui->pteFraction->appendPlainText(tr("Adds the fractional numbers\n"));
            QVector<qint64>::iterator nt;
            QVector<qint64>::iterator dt;

            for(nt = numeratorArray->begin(), dt = denominatorArray->begin(); nt < numeratorArray->end(); nt++, dt++) {
                if(nt + 1 < numeratorArray->end()) {
                    ui->pteFraction->moveCursor(QTextCursor::EndOfLine);
                    ui->pteFraction->insertPlainText(QString::number(*nt) + '/' + QString::number(*dt) + " + ");
                } else {
                    ui->pteFraction->moveCursor(QTextCursor::End);
                    ui->pteFraction->insertPlainText(QString::number(*nt) + '/' + QString::number(*dt));
                }
            }

            ui->pteFraction->moveCursor(QTextCursor::End);
            ui->pteFraction->insertPlainText(" = " + QString::number(summa) + '/' + QString::number(lcd));
            // ui->pteFraction->appendPlainText(tr("Sum = ") + QString::number(summa) + '/' + QString::number(lcd));
            shortenFractionalNumbers(summa, lcd);
            delete[] numerator;
            return;
        }
    }
}
void MainWindow::shortenFractionalNumbers(qint64 numerator, qint64 denominator)
{
    if((numerator %  denominator) == 0) {
        ui->pteFraction->appendPlainText(tr("Sum = ") + QString::number(numerator / denominator));
        return;
    }

    //const qint64 hogst = numerator > denominator ? numerator : denominator;
    const qint64 minst = numerator < denominator ? numerator : denominator;
    qint64 num = 0, den = 0, heltal = 0, del = 0;

    for(qint64 i = 2; i <= minst; i++) {
        if(((numerator % i) == 0) && (((denominator % i) == 0))) {
            num = numerator / i;
            den = denominator / i;
            ui->pteFraction->appendPlainText(tr("Shortens... divided by ") + QString::number(i) + tr(" gets... ") + QString::number(num) + '/' + QString::number(den));
        }
    }

    //qDebug() << "heltal=" << heltal << " del=" << del;
    double approximate;

    if(num == 0 && den == 0) {
        heltal = numerator / denominator;
        del = numerator % denominator;
        approximate = double(numerator) / double(denominator) + heltal;

        if(heltal != 0) {
            if(heltal > 0)
                ui->pteFraction->appendPlainText(tr("Sum = ") + QString::number(numerator) + '/' + QString::number(denominator) + " = " + QString::number(heltal) + '+' + QString::number(del) + '/' + QString::number(denominator) + " \n" + tr("Approximate sum") + " = " + QString::number(approximate));
            else
                ui->pteFraction->appendPlainText(tr("Sum = ") + QString::number(numerator) + '/' + QString::number(denominator) + " = -(" + QString::number(-1 * heltal) + '+' + QString::number(-1 * del) + '/' + QString::number(denominator) + ")\n" + tr("Approximate sum") + " = "  + QString::number(approximate));
        } else
            ui->pteFraction->appendPlainText(tr("Sum = ") + QString::number(numerator) + '/' + QString::number(denominator) + " \n" + tr("Approximate sum") + " = " + QString::number(approximate));
    } else {
        heltal = num / den;
        del = num % den;
        approximate = double(numerator) / double(denominator) + heltal;

        if(heltal > 0)
            ui->pteFraction->appendPlainText(tr("Sum = ") + QString::number(numerator) + '/' + QString::number(denominator) + " = " + QString::number(num) + '/' + QString::number(den) + " = " + QString::number(heltal) + '+' + QString::number(del) + '/' + QString::number(den) + " \n" + tr("Approximate sum") + " = "  + QString::number(approximate));
        else
            ui->pteFraction->appendPlainText(tr("Sum = ") + QString::number(numerator) + '/' + QString::number(denominator) + " = " + QString::number(num) + '/' + QString::number(den) + " \n" + tr("Approximate sum") + " = " + QString::number(approximate));
    }
}


