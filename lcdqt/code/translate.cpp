/*
    lcd
    Copyright (C) 2018 - 2020 Ingemar Ceicer
    http://ceicer.org/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/
#include "mainwindow.h"
#include "ui_mainwindow.h"

// Language
void MainWindow::english()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, PROG_NAME, PROG_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("Language");
    QString value = settings.value("lang", "nothing").toString();

    if(value != "en_US") {
        switch(QMessageBox::information(this, PROG_NAME " " VERSION,
                                        tr("The program must be restarted for the new language settings to take effect."),
                                        tr("Restart Now"),
                                        tr("Cancel"), nullptr, 2)) {
            case 0:
                settings.setValue("lang", "en_US");
                //  bool exit = setEndConfig();
                // if(exit) {
                //     return;
                // }
                // QApplication::quit();
                startaom();
        }
    }

    settings.endGroup();
}
void MainWindow::swedish()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, PROG_NAME, PROG_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("Language");
    QString value = settings.value("lang", "nothing").toString();

    if(value != "sv_SE") {
        switch(QMessageBox::information(this, PROG_NAME  " " VERSION,
                                        tr("The program must be restarted for the new language settings to take effect."),
                                        tr("Restart Now"),
                                        tr("Cancel"), nullptr, 2)) {
            case 0:
                settings.setValue("lang", "sv_SE");
                //  bool exit = setEndConfig();
                // if(exit) {
                //    return;
                // }
                // QApplication::quit();
                startaom();
        }
    }

    settings.endGroup();
}

void MainWindow::startaom()
{
    setEndConfig();
    const QString EXECUTE =
        QCoreApplication::applicationDirPath() + "/" +
        QFileInfo(QCoreApplication::applicationFilePath()).fileName();
    QProcess p;
    p.setProgram(EXECUTE);
    p.startDetached();
    close();
}
