/*
    lcd
    Copyright (C) 2018 - 2020 Ingemar Ceicer
    http://ceicer.org/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

#include "mainwindow.h"
#include <QTranslator>
int main(int argc, char *argv[])
{
    qputenv("QT_STYLE_OVERRIDE", nullptr);
    auto *a = new QApplication(argc, argv);
    QFontDatabase::addApplicationFont(":/fonts/PTMono-Regular.ttf");
#ifdef Q_OS_LINUX     // Linux
    QFont f = QFont("PT Mono", LINUX_FONT, 1);
#endif
#ifdef Q_OS_WIN // Windows 32- and 64-bit
    QFont f = QFont("PT Mono", WINDOWS_FONT, 1);
#endif
    a->setFont(f);
    // const QString INSTALL_DIR = QDir::toNativeSeparators(a->applicationDirPath() + "/");
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, PROG_NAME, PROG_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("Language");
    QString sp = settings.value("lang", "nothing").toString();
    // qDebug() << value;
    settings.endGroup();
    QTranslator translator;
    const QString locale = QLocale::system().name();
    const QString translationPath = ":/i18n/translation";
    //qDebug() << "value " << translationPath + "_" + sp + ".qm";
    //qDebug() << "locale " << translationPath + "_" + locale + ".qm";

    if(sp  == "") {
        sp = QLocale::system().name();

        if(translator.load(translationPath + "_" + sp + ".qm")) {
            QApplication::installTranslator(&translator);
            settings.beginGroup("Language");
            settings.setValue("language", sp);
            settings.endGroup();
        } else {
            if(sp != "en_US")
                QMessageBox::information(nullptr, PROG_NAME " " VERSION, PROG_NAME " is unfortunately not yet translated into your language, the program will start with English menus.");

            settings.beginGroup("Language");
            settings.setValue("language", sp);
            settings.endGroup();
        }
    } else {
        if(translator.load(translationPath + "_" + sp + ".qm"))
            QApplication::installTranslator(&translator);
    }

    auto *w = new MainWindow;
    w->show();
    return a->exec();
}
