<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>CheckForUpdates</name>
    <message>
        <location filename="../checkforupdates.cpp" line="40"/>
        <location filename="../checkforupdates.cpp" line="84"/>
        <source>No Internet connection was found.
Please check your Internet settings and firewall.</source>
        <translation>Ingen internetanslutning hittas
Kolla dina nätverksinställningar och brandvägg.</translation>
    </message>
    <message>
        <source>LCD</source>
        <translation type="vanished">MGN</translation>
    </message>
    <message>
        <source>MGN</source>
        <translation type="vanished">MGN</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="46"/>
        <location filename="../checkforupdates.cpp" line="89"/>
        <source>
There is a later version of </source>
        <translation>
Det finns en senare version av </translation>
    </message>
    <message>
        <source>Please download from</source>
        <translation type="vanished">Ladda ner från</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="46"/>
        <location filename="../checkforupdates.cpp" line="89"/>
        <source>Latest version: </source>
        <translation>Senaste versionen: </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="46"/>
        <location filename="../checkforupdates.cpp" line="89"/>
        <source>Please </source>
        <translation>Var vänlig </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="46"/>
        <location filename="../checkforupdates.cpp" line="89"/>
        <source>Download</source>
        <translation>Ladda ner</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="50"/>
        <source>
Your version of </source>
        <translation>
Din version av </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="50"/>
        <source> is later than the latest official version </source>
        <translation> är nyare än den senaste officiella versionen </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="54"/>
        <source>
You have the latest version of </source>
        <translation>
Du har den senaste versionen av </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="58"/>
        <location filename="../checkforupdates.cpp" line="91"/>
        <source>
There was an error when the version was checked.</source>
        <translation>
Det uppstod ett fel när versionen kontrollerades.</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="130"/>
        <source>New updates:</source>
        <translation>Nya uppdateringar:</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="81"/>
        <location filename="../mainwindow.ui" line="211"/>
        <source>Add</source>
        <translation>Lägg till</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="88"/>
        <location filename="../mainwindow.ui" line="216"/>
        <source>LCD</source>
        <translation>MGN</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="95"/>
        <location filename="../mainwindow.ui" line="230"/>
        <source>Clear</source>
        <translation>Rensa</translation>
    </message>
    <message>
        <source>File</source>
        <translation type="vanished">Arkiv</translation>
    </message>
    <message>
        <source>Language</source>
        <translation type="vanished">Språk</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="vanished">Hjälp</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="181"/>
        <source>&amp;File</source>
        <translation>&amp;Arkiv</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="190"/>
        <source>&amp;Language</source>
        <translation>&amp;Språk</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="197"/>
        <source>&amp;Help</source>
        <translation>&amp;Hjälp</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="225"/>
        <source>Exit</source>
        <translation>Avsluta</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="239"/>
        <source>English</source>
        <translation>Engelska</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="248"/>
        <source>Swedish</source>
        <translation>Svenska</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="257"/>
        <source>Check for updates</source>
        <translation>Kolla efter uppdateringar</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="266"/>
        <source>About</source>
        <translation>Om</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="275"/>
        <source>License</source>
        <translation>License</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="286"/>
        <source>Check for software updates at program startup</source>
        <translation>Sök efter programuppdateringar vid programstart</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="137"/>
        <source>Source code
</source>
        <translation>Källkod
</translation>
    </message>
    <message>
        <source>LCD </source>
        <oldsource>lcd </oldsource>
        <translation type="vanished">MGN </translation>
    </message>
    <message>
        <source> Copyright (C) 2018 Ingemar Ceicer. This is free software; see the GNU General Public Licence for copying conditions. There is NO warranty.
Least Common Denominator (LCD)</source>
        <translation type="vanished"> Upphovsrätt (C) 2018 Ingemar Ceicer. Detta är fri programvara; se GNU General Public License för kopieringsvillkor. Det finns ingen garanti.
Minsta Gemensamma Nämnare (MGN)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="137"/>
        <source> Copyright (C) 2018 </source>
        <translation> Copyright (C) 2018 </translation>
    </message>
    <message>
        <source> Ingemar Ceicer. This is free software; see the GNU General Public Licence for copying conditions. There is NO warranty.
Least Common Denominator (LCD)</source>
        <translation type="vanished"> Ingemar Ceicer. Detta är fri programvara; se GNU General Public License för kopieringsvillkor. Det finns ingen garanti.
Minsta Gemensamma nämnare (MGN)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="47"/>
        <location filename="../mainwindow.cpp" line="48"/>
        <location filename="../mainwindow.cpp" line="49"/>
        <location filename="../mainwindow.cpp" line="183"/>
        <location filename="../mainwindow.cpp" line="184"/>
        <location filename="../mainwindow.cpp" line="185"/>
        <source>Division by zero!</source>
        <translation>Division med noll!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="137"/>
        <source> Ingemar Ceicer.
This is free software; see the GNU General Public Licence
for copying conditions. There is NO warranty.
Least Common Denominator (LCD)</source>
        <translation> Ingemar Ceicer.
Dett är fri mjukvara; se GNU General Public License
för koppieringsrättigheter. Det finns INGEN garanti.
Minsta Gemensamma Nämnare (MGN)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="143"/>
        <source>Homepage</source>
        <translation>Hemsida</translation>
    </message>
    <message>
        <source>Overflow</source>
        <translation type="vanished">För stort tal</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="237"/>
        <source>Overflow (Denominator)</source>
        <translation>För stort tal (Nämnaren)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="260"/>
        <source>LCD = </source>
        <translation>MGN = </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="261"/>
        <source>Adds the fractional numbers
</source>
        <translation>Adderar bråktalen
</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="287"/>
        <location filename="../mainwindow.cpp" line="313"/>
        <location filename="../mainwindow.cpp" line="315"/>
        <location filename="../mainwindow.cpp" line="317"/>
        <location filename="../mainwindow.cpp" line="324"/>
        <location filename="../mainwindow.cpp" line="326"/>
        <source>Sum = </source>
        <translation>Summa = </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="299"/>
        <source>Shortens... divided by </source>
        <translation>Förkortar... dividerar med </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="299"/>
        <source> gets... </source>
        <translation> ger... </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="313"/>
        <location filename="../mainwindow.cpp" line="315"/>
        <location filename="../mainwindow.cpp" line="317"/>
        <location filename="../mainwindow.cpp" line="324"/>
        <location filename="../mainwindow.cpp" line="326"/>
        <source>Approximate sum</source>
        <translation>Ungefärlig summa</translation>
    </message>
    <message>
        <source>Approximate value</source>
        <translation type="vanished">Ungefärligt värde</translation>
    </message>
    <message>
        <location filename="../translate.cpp" line="30"/>
        <location filename="../translate.cpp" line="55"/>
        <source>The program must be restarted for the new language settings to take effect.</source>
        <translation>Programet måste startas om för att de nya språkinställningarna skall gälla.</translation>
    </message>
    <message>
        <location filename="../translate.cpp" line="31"/>
        <location filename="../translate.cpp" line="56"/>
        <source>Restart Now</source>
        <translation>Starta om nu</translation>
    </message>
    <message>
        <location filename="../translate.cpp" line="32"/>
        <location filename="../translate.cpp" line="57"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="../system.cpp" line="23"/>
        <source>This program uses Qt version </source>
        <translation>Detta program använder Qt version </translation>
    </message>
    <message>
        <location filename="../system.cpp" line="23"/>
        <source> running on </source>
        <translation> körs på </translation>
    </message>
    <message>
        <location filename="../system.cpp" line="25"/>
        <source> was created </source>
        <translation> skapades </translation>
    </message>
    <message>
        <location filename="../system.cpp" line="25"/>
        <source>by a computer with</source>
        <translation>av en dator med</translation>
    </message>
    <message>
        <location filename="../system.cpp" line="34"/>
        <location filename="../system.cpp" line="36"/>
        <location filename="../system.cpp" line="106"/>
        <source> Compiled by</source>
        <translation> Kompilerad med</translation>
    </message>
    <message>
        <source>Compiled by </source>
        <translation type="vanished">Kompilerad med </translation>
    </message>
    <message>
        <source>Compiled by</source>
        <translation type="vanished">Kompilerad med</translation>
    </message>
    <message>
        <location filename="../system.cpp" line="42"/>
        <source>Full version number </source>
        <translation>Fullständigt versionsnummer </translation>
    </message>
    <message>
        <location filename="../system.cpp" line="102"/>
        <source>Unknown version</source>
        <translation>Okänd version</translation>
    </message>
    <message>
        <location filename="../system.cpp" line="108"/>
        <source>Unknown compiler.</source>
        <translation>Okänd komilator.</translation>
    </message>
</context>
</TS>
