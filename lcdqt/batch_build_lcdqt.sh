#!/bin/bash

B=$(tput bold)
N=$(tput sgr0)

executable=lcdqt
QT5=5.15.12
QT6=6.6.2

if [ `getconf LONG_BIT` = 64 ]; then
	GCC="gcc_64"
else 
	GCC="gcc_32"
fi
possibleappdirs=$($(echo ls -d AppDir*))

for i in ${possibleappdirs[*]}; do

    options+=(${i})

done

PS3='Please select a folder to copy executable to: '
select opt in "${options[@]}" "Quit"; do
    case "$REPLY" in
    $((${#options[@]} + 1)))
        echo "Goodbye!"
        exit 0
        break
        ;;

    esac
    [ $REPLY -gt $((${#options[@]} + 1)) -o $REPLY -lt 1 ] && echo "Invalid selection" || break
done

echo "You chose $opt which is $(pwd)/${options[(($REPLY - 1))]}"

appdirname="${options[(($REPLY - 1))]}"

if [ -d "$(pwd)/$appdirname" ]; then
    echo "You have chosen to copy $executable to $(pwd)/$appdirname/usr/bin"

else
    echo "Error: Directory $(pwd)/$appdirname does not exists."

fi

echo -----------------------------------------------------------
PS3='Please select the Qt version to use when compiling: '
options=("Qt5.15.2" "Qt${QT5}" "Qt${QT6}" "Quit")
select opt in "${options[@]}"; do
    case $opt in
    "Qt5.15.2")
        echo "You chose choice $REPLY which is $opt"
        qmakePath="/opt/Qt/5.15.2/${GCC}/bin/qmake"
        build_executable="build-executable5"
        export LD_LIBRARY_PATH=/opt/Qt/5.15.2/lib:$LD_LIBRARY_PATH
        export PATH=/opt/Qt/5.15.2/bin:$PATH
        break
        ;;
    "Qt${QT5}")
        echo "You chose choice $REPLY which is $opt"
        qmakePath="/opt/Qt/${QT5}/${GCC}/bin/qmake"
        build_executable="build-executable5"
        export LD_LIBRARY_PATH=/opt/Qt/${QT5}/${GCC}/lib:$LD_LIBRARY_PATH
        export PATH=/opt/Qt/${QT5}/${GCC}/bin:$PATH
        break
        ;;
    "Qt${QT6}")
        echo "You chose choice $REPLY which is $opt"
        qmakePath="/opt/Qt/${QT6}/${GCC}/bin/qmake"
        build_executable="build-executable6"
        export LD_LIBRARY_PATH=/opt/Qt/${QT6}/${GCC}/lib:$LD_LIBRARY_PATH
        export PATH=/opt/Qt/${QT6}/${GCC}/bin:$PATH
        break
        ;;
    "Quit")
        break
        ;;
    *) echo "invalid option $REPLY" ;;
    esac
done

echo -----------------------------------------------------------
PS3='Please enter your choice: '
options=("Debug" "Release" "Quit")
select opt in "${options[@]}"; do
    case $opt in
    "Debug")
        echo "You chose choice $REPLY which is $opt"
        mkdir build
        cd build
        $qmakePath -project ../code/*.pro
        $qmakePath ../code/*.pro -spec linux-g++ CONFIG+=debug CONFIG+=qml_debug && /usr/bin/make qmake_all
        make -j$(nproc)
        make clean -j$(nproc)
        cd ..
        rm -r build
        break
        ;;
    "Release")
        echo "You chose choice $REPLY which is $opt"
        mkdir build
        cd build
        $qmakePath -project ../code/*.pro
        if [ $(getconf LONG_BIT) -eq "64" ]; then
            echo -e "${B}64-bit${N}"
            $qmakePath ../code/*.pro -spec linux-g++ CONFIG+=qtquickcompiler && /usr/bin/make qmake_all
        else
            echo -e "${B}32-bit${N}"
            $qmakePath ../code/*.pro -spec linux-g++ CONFIG+=release CONFIG+=qml_release && /usr/bin/make qmake_all
        fi

        /usr/bin/make -j$(nproc)
        /usr/bin/make clean -j$(nproc)
        cd ..
        rm -r build

        cp -f $(pwd)/$build_executable/$executable $(pwd)/$appdirname/usr/bin/

        #
        break
        ;;
    "Quit")
        break
        ;;
    *) echo "invalid option $REPLY" ;;
    esac
done

# AppImage
echo -----------------------------------------------------------
PS3='Please enter your choice: '
options=("Run this script again" "Build AppImage" "Do not build AppImage" "Quit")
select opt in "${options[@]}"; do
    case $opt in
    "Run this script again")
        echo "You chose choice $REPLY which is $opt"
        ./$(basename "$0")
        break
        ;;
    "Build AppImage")
        echo "You chose choice $REPLY which is $opt"
        ./build_appimage.sh
        break
        ;;
    "Do not build AppImage")
        echo "You chose choice $REPLY which is $opt"
        echo "Goodbye"
        exit 0
        break
        ;;
    "Quit")
        break
        exit 0
        ;;
    *)
        echo "invalid option $REPLY"
        ;;
    esac
done
