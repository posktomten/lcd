/* ><(((*>  ><(((*>  ><(((*>  ><(((*>  ><(((*>  ><(((*> ><(((*>  ><(((*>

      LCD Copyright 2018 Ingemar Ceicer
      http://ceicer.org
      programmeing1@ceicer.org


      LCD is free software: you can redistribute it and/or modify
      it under the terms of the GNU General Public License as published by
      the Free Software Foundation, either version 3 of the License, or
      (at your option) any later version.

      Foobar is distributed in the hope that it will be useful,
      but WITHOUT ANY WARRANTY; without even the implied warranty of
      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
      GNU General Public License for more details.

      You should have received a copy of the GNU General Public License
      along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

      ><(((*>  ><(((*>  ><(((*>  ><(((*>  ><(((*>  ><(((*> ><(((*>  ><(((*> */

#include <QCoreApplication>
#include <QTranslator>
#include <QObject>
#include <QDir>
//#include "checkforupdates.h"
#include <iostream>
using namespace std;

void lcd(qint64[], qint64[], int);
void shortenFractionalNumbers(qint64 numerator, qint64 denominator);
#define MAX_FACTOR 9223372036854775807 // 8 byte - 1
#define VERSION " 1.0.2 "
//#define MAX_FACTOR 2147483646 // 4 byte - 1
int main(int argc, char *argv[])
{
#if defined (Q_OS_WIN)
    //Unicode (UTF-8)
    system("chcp 65001");
#endif
    QCoreApplication a(argc, argv);
    short sprakval = 0;
    short sprak = 0;

    do {
        cout << "Select language / välj språk" << endl;
        cout << "1. English" << endl;
        cout << "2. Svenska" << endl;
        cout << ":>";
        cin >> sprakval;

        switch(sprakval) {
            case 1:
                sprak = 1;
                break;

            case 2:
                sprak = 2;
                break;

            default:
                cout << "Incorrect selection / Felaktigt val" << endl;
        }
    } while(sprakval != 1 && sprakval != 2);

    QTranslator *translator;

    if(sprak == 2) {
        translator = new QTranslator;
        const QString LANGUAGEFILE = QDir::toNativeSeparators(QCoreApplication::applicationDirPath() + "/" + "translation_sv_SE.qm");
        translator->load(LANGUAGEFILE);
        a.installTranslator(translator);
    }

    cout << "lcd" VERSION << QObject::tr("Copyright (C) 2018 Ingemar Ceicer.").toStdString() + '\n';
    cout << QObject::tr("This is free software; see the GNU General Public Licence").toStdString() + '\n';
    cout << QObject::tr("for copying conditions. There is NO warranty.").toStdString() + '\n';
    cout << QObject::tr("Least Common Denominator (LCD)").toStdString() << '\n';
    cout << QObject::tr("Enter the number of fractional numbers").toStdString() + ":>";
    qint64 numerator, denominator;
    int number, counter = 0, i = 0;
    cin >> number;

    if(number < 1)
        return 0;

    qint64 *numeratorArray = new qint64[number];
    qint64 *denominatorArray = new qint64[number];

    while(counter < number) {
        cout << QObject::tr("Fraction ").toStdString() << ++counter << QObject::tr(": numerator :>").toStdString();
        cin >> numerator;
        numeratorArray[i] = numerator;
        cout << QObject::tr("Fraction ").toStdString() << counter << QObject::tr(": denominator :>").toStdString();
        cin >> denominator;
        denominatorArray[i] = denominator;
        i++;
    }

    cout << endl;
    lcd(numeratorArray, denominatorArray, counter);
    delete[] numeratorArray;
    delete[] denominatorArray;
    cout << endl;
#if defined (Q_OS_WIN)
    system("pause");
#endif
    return 0;
    //return a.exec();
}
void lcd(qint64 numeratorArray[], qint64 denominatorArray[], int counter)
{
    // Find the fractional number with the largest denominator
    qint64 factor, highest = 0, found, lcd = 0, summa;
    qint64 *numerator = new qint64[counter];
    // Practical test
    // double a1, b1, a2, b2;

    for(qint64 i = 0; i < counter; i++) {
        if(denominatorArray[i] > highest) {
            highest = denominatorArray[i];
        }
    }

    for(factor = 1; factor < MAX_FACTOR; factor++) {
        found = 0;

        for(qint64 i = 0; i < counter; i++) {
            if((((highest * factor)  %  denominatorArray[i]) == 0)) {
                lcd = highest * factor;
                numerator[i] =  numeratorArray[i] * (lcd / denominatorArray[i]);
                found++;
            } else
                break;
        }

        // If the LCD is found
        if(found == counter) {
            summa = 0;

            for(qint64 i = 0; i < counter; i++) {
                summa += numerator[i];
                cout << numeratorArray[i] << '/' << denominatorArray[i] << " = " << numerator[i] << '/' << lcd << endl;
                // Practical test
                /*  a1 = (double)numeratorArray[i];
                  b1 = (double)denominatorArray[i];
                  a2 = (double)numerator[i];
                  b2 = (double)lcd;
                  cout << a1 / b1 << '=' << a2 / b2 << endl; */
            }

            cout << QObject::tr("LCD").toStdString() + " = " << lcd << endl;

            for(int i = 0; i < counter; i++) {
                if((i + 1) < counter)
                    cout << numeratorArray[i] << '/' << denominatorArray[i] << " + ";
                else
                    cout << numeratorArray[i] << '/' << denominatorArray[i];
            }

            cout << " = " << summa << '/' << lcd << endl;
            shortenFractionalNumbers(summa, lcd);
            delete[] numerator;
            return;
        }
    }
}
void shortenFractionalNumbers(qint64 numerator, qint64 denominator)
{
    if((numerator %  denominator) == 0) {
        cout << QObject::tr("Sum").toStdString() + " = " << numerator / denominator;
        return;
    }

    //const qint64 hogst = numerator > denominator ? numerator : denominator;
    const qint64 minst = numerator < denominator ? numerator : denominator;
    qint64 num = 0, den = 0, heltal = 0, del = 0;

    for(qint64 i = 2; i <= minst; i++) {
        if(((numerator % i) == 0) && (((denominator % i) == 0))) {
            num = numerator / i;
            den = denominator / i;
            cout << QObject::tr("Shortens... divided by ").toStdString() << i << QObject::tr(" gets... ").toStdString() << num << '/' << den << endl;
        }
    }

    if(num == 0 && den == 0) {
        heltal = numerator / denominator;
        del = numerator % denominator;

        if(heltal > 0)
            cout << QObject::tr("Sum").toStdString() + " = " << numerator << '/' << denominator << " = " << heltal << '+' << del << '/' << denominator << endl;
        else
            cout << QObject::tr("Sum").toStdString() + " = " << numerator << '/' << denominator << endl;
    } else {
        heltal = num / den;
        del = num % den;

        if(heltal > 0)
            cout  << QObject::tr("Sum").toStdString() + " = " << numerator << '/' << denominator << " = " << num << '/' << den << " = " << heltal << '+' << del << '/' << den << endl;
        else
            cout << QObject::tr("Sum").toStdString() + " = " << numerator << '/' << denominator << " = " << num << '/' << den << endl;
    }
}
